import React, {Component} from 'react';

class RegistrationForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            firstName: "",
            lastName: "",
            npi: "",
            address: "",
            phone: "",
            email: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit(event) {
        console.log(`
              First Name: ${this.state.firstName}
              Last Name: ${this.state.lastName}
              NPI: ${this.state.npi}
              Address: ${this.state.address}
              Phone Number: ${this.state.phone}
              Email: ${this.state.email}
        `);

        event.preventDefault();

    }

    render() {


        return (
            <form method="post" onSubmit={this.handleSubmit}>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label className="form-label">First Name:</label>
                        <input
                            name="firstName"
                            type="text"
                            value={this.state.firstName}
                            onChange={this.handleChange}
                            className="form-control"
                            required/>
                    </div>
                    <div className="form-group col-md-6">
                        <label className="form-label">Last Name:</label>
                        <input
                            name="lastName"
                            type="text"
                            value={this.state.lastName}
                            onChange={this.handleChange}
                            className="form-control"
                            required/>
                    </div>
                </div>
                <div className="form-group">
                    <label className="form-label">NPI Number:</label>
                    <input
                        name="npi"
                        type="number"
                        value={this.state.npi}
                        onChange={this.handleChange}
                        className="form-control"
                        required/>
                </div>
                <div className="form-group">
                    <label className="form-label">Business Address:</label>
                    <input
                        name="address"
                        type="text"
                        value={this.state.address}
                        onChange={this.handleChange}
                        className="form-control"
                        required/>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label className="form-label">Telephone Number:</label>
                        <input
                            name="phone"
                            type="tel"
                            value={this.state.phone}
                            pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            placeholder={"XXX-XXX-XXXX"}
                            onChange={this.handleChange}
                            className="form-control"
                            required/>
                    </div>
                    <div className="form-group col-md-6">
                        <label className="form-label">Email:</label>
                        <input
                            name="email"
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                            className="form-control"
                            required/>
                    </div>
                </div>

                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        );
    }
}

export default RegistrationForm;