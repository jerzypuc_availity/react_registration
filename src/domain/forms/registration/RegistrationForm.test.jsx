import React from 'react';
import { mount } from 'enzyme';
import RegistrationForm from './RegistrationForm';

describe("RegistrationForm", () => {
    it('calls handleSubmit when form is submitted', () => {
        const handleSubmitFn = jest.spyOn(RegistrationForm.prototype, 'handleSubmit');
        const wrapper = mount(<RegistrationForm />);

        wrapper.find('form').simulate('submit');

        expect(handleSubmitFn).toHaveBeenCalled();
    })
})
