import React from 'react';
import './App.css';
import RegistrationForm from './domain/forms/registration/RegistrationForm';

function App() {

    return (
        <div className="col-md-6">
            <h1>Register User</h1>
            <RegistrationForm/>
        </div>
    );
}

export default App;
